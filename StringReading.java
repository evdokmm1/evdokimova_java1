import java.util.Scanner;

public class StringReading {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userString = scanner.nextLine();
        int sum = 0;
        String[] words = userString.split(" ");
        for (String word : words) {
            char a = word.charAt(0);
            if (a >= 'A' && a <= 'Z') {
                sum++;
            }
        }
    }
}