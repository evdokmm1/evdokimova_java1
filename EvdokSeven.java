package HW;

import java.util.Scanner;

public class EvdokSeven {
    public void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = getArray();
        System.out.println(toString(array));
        bubbleSort(array);
    }

    private void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int elem = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = elem;
                }
            }
        }
    }

    private String toString(int[] array) {
        StringBuilder answer = new StringBuilder("[");
        for (int elem : array) {
            answer.append(elem);
            answer.append(", ");
        }
        answer.append("]");
        return answer.toString();
    }

    private int[] getArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите кол-во элементов - ");
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите элемент - ");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}
