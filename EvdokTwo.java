package HW;

import java.util.Scanner;

public class EvdokTwo {
    public static void main(String[] args) {
//        byte k = 2;
//        short t = 25;
//        int r = 100;
//        long l = 150L;
//        float f = 3.5f;
//        double d = 3.55;
//        char d = 'd';
//
//        int sum = k + t;
//        int raz = k - t;
//        int mil = k * t;
//        int del = r / t;
//
//        //r++
//        r = r + 1;
//
//        //r--
//        r = r - 1;
//
//        //r = r / t
//        r /= t;
//
//        //r = r * t
//        r *= t;
//
//        //r = r - t
//        r -= t;
//
//        //r = r + t
//        r += t;
//
//        //остаток от деления
//        System.out.println(t % 2);
//
//        System.out.println((t % 2) == 0);
//
//        boolean dn = true;
//        dn = false;
//
//        //больше
//        System.out.println(k > 2);
//        //меньше
//        System.out.println(k < 2);
//        //больше или равно
//        System.out.println(k >= 2);
//        //меньше или равно
//        System.out.println(k <= 2);
//        //равно
//        System.out.println(k == 2);
//        //не равно
//        System.out.println(k != 2);
//        //отрицание
//        System.out.println(!dn);
//        //эквивалентно равно
//        System.out.println(!(k != 2));
//
//        System.out.println((k > 2) & (k < 5));
//        System.out.println((k > 2) | (k < 5));
//        //ленивый оператор
//        System.out.println((k > 2) && (k < 5));
//        System.out.println((k > 2) || (k < 5));
//

        Scanner s = new Scanner(System.in);
        System.out.print("Введите число - ");
        int k = s.nextInt();
        System.out.println((k > 0) & (k < 9));
        System.out.println((k > 2) & (k < 6));
        System.out.println(k * 2);
    }
}