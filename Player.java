package HW;

public class Player {
    public static void main(String[] args) {
        int hp;
        String userName;

        public Player(String userName; int hp) {
            this.hp = 100;
            this.userName = userName;
        }

        public int getHp(int hp) {
            return hp;
        }

        public String getUserName() {
            return userName;
        }

        public String setUserName(String userName){
            this.userName = userName;
        }

        @Override
        public String toString() {
            return this.userName + "\n" + this.hp;
        }
    }
}
